/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Agente;
import modelo.Categoria;
import modelo.OrganismoTransito;
import modelo.Persona;
import modelo.Vehiculo;
import ventanas.OrganismoUI;

/**
 *
 * @author salasistemas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        OrganismoTransito organismo = new OrganismoTransito();

        try {
            //objetos de pruebas
            organismo.registarAgente(new Agente(3044794480L, 1113624763, "mario", " perez"));
            organismo.registarAgente(new Agente(3005009923L, 9876543210L, "Rodolfo", "Bolanos"));
            organismo.registrarVehiculo(new Vehiculo("SPD30B", "Toyota", (short) 2015, "NEGRO"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date fecha = sdf.parse("2020/12/31");
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.A1);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.A2);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.B1);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.B2);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.B3);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.C1);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(1113624763, "Rodolfo", "Bolanos"), fecha, Categoria.C2);
            organismo.expedirLicenciaPorPrimeraVez(new Persona(3044794480L, "Karen", "Gonzalez"), fecha, Categoria.C2);

        } catch (Exception exc) {

        }

        OrganismoUI organismoTransito = new OrganismoUI(organismo);
        organismoTransito.setVisible(true);

        // TODO code application logic here
    }

}
