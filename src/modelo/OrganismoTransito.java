package modelo;

import java.time.Instant;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;

import java.util.LinkedList;
import java.util.List;
import sun.util.calendar.LocalGregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author salasistemas
 */
public class OrganismoTransito {

    private List<Agente> guarda = new LinkedList<>();
    private List<Multa> fine = new LinkedList<>();
    private List<MotivoMulta> due = new LinkedList<>();
    private List<Vehiculo> car = new LinkedList<>();
    private List<Licencia> license = new LinkedList<>();

    //metodo para buscar la placa en la lista vehiculos
    public Vehiculo buscarVehiculo(String placa) throws Exception {

        for (Vehiculo car : this.car) {
            if (car.getPlaca().contains(placa)) {
                return car;
            }
        }

        return null;

    }

//metodo para registrar vehiculo
    public void registrarVehiculo(Vehiculo car) throws Exception {
        //esta excepcion es para verficar si la placa del vehiculo a registar 
        // ya se encuentra contenida en la lista de vehiculos
        // si la lista esta vacia, agrega el vehiculo
        if ((buscarVehiculo(car.getPlaca()) == null)) {
            this.car.add(car);
        } else {
            throw new Exception("La placa del vehiculo ya se "
                    + "encuentra registrada");

        }

    }

    //metodo para buscar  Agente 
    public Agente buscarAgente(long placa) {

        for (Agente agent : this.guarda) {
            if (agent.getPlacaAgente() == placa) {
                return agent;

            }
        }
        return null;
    }

    //metodo para registrar agente de transito
    public void registarAgente(Agente guarda) throws Exception {
        if (buscarAgente(guarda.getPlacaAgente()) == null) {
            this.guarda.add(guarda);
        } else {
            throw new Exception("El guarda ya se encuentra registrado");
        }
    }
//    public void registrarVehiculo(Vehiculo vehiculos) throws Exception {
//        if (this.vehiculos.contains(vehiculos)) {
//            throw new Exception("El mecanico ya esta registrado");
//        } else {
//            this.vehiculos.add(vehiculos);
//        }
//    }

    public List<Licencia> buscarLicencias(long identificacion) {
        LinkedList<Licencia> encontradas = new LinkedList<>();
        for (Licencia licencia : this.license) {
            if (licencia.getDriver().getIdentificacion() == identificacion) {
                encontradas.add(licencia);
            }
        }
        return encontradas;
    }

    //Metodo que busca un conductor y licencias por su identificacion
    public LinkedList<Licencia> buscarConductorYSusLicencias(long identificacion) {
        LinkedList<Licencia> find = new LinkedList();
        for (int i = 0; i < license.size(); i++) {
            if (license.get(i).getDriver().getIdentificacion() == identificacion) {
                find.add(license.get(i));
            }
        }
        return find;
    }

    //Metodo que expida una licencia por segunda vez
    public void expedirLicencia2(Persona driver, Date fechaDeExp, Categoria type) throws Exception {
        LinkedList<Licencia> foundCAL = buscarConductorYSusLicencias(driver.getIdentificacion());
        boolean ready = false;

        if (foundCAL.isEmpty()) {
            throw new Exception("La persona con el ID: " + driver.getIdentificacion() + ". No esta registrada.");
        }

        for (int i = 0; i < foundCAL.size() && !ready; i++) {
            if (foundCAL.get(i).getType() == type) {
                ready = true;
            }
        }

        if (!ready) {
            Licencia nuevaL = new Licencia(foundCAL.get(0).getDriver(), fechaDeExp, type);
            license.add(nuevaL);
        } else {
            throw new Exception("Ya tiene una licencia con el tipo: " + type);
        }

    }

    //Metodo que registra una persona con su respectiva licencia por primera vez
    public void expedirLicenciaPorPrimeraVez(Persona driver, Date fecha, Categoria type) throws Exception {
        license.add(new Licencia(driver, fecha, type));
    }

    //Metodo que renueva una licencia
    public void renovarLiencia(Persona driver, Categoria type, Date fechaNueva) throws Exception {
        LinkedList<Licencia> lista = buscarConductorYSusLicencias(driver.getIdentificacion());
        if (lista.isEmpty()) {
            throw new Exception("La persona con el ID: " + driver.getIdentificacion() + ". No esta registrada.");
        }
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getType() == type) {
                if ((((((double) (fechaNueva.getTime()) / 1000.0) / 3600.0) / 24.0) / 365.0) - (((((double) (Date.from(Instant.now()).getTime()) / 1000.0) / 3600.0) / 24.0) / 365.0) >= 1.0 && lista.get(i).getFechaExpedicion().getTime() - Date.from(Instant.now()).getTime() < 0) {
                    lista.get(i).getFechaExpedicion();
                } else {
                    Date ayer = new Date();

                    throw new Exception("La nueva fecha no puede ser menor a la actual , la nueva fecha tiene que ser posterior a la fecha actual por 1 año y la licencia tiene que estar vencida");
                }

            }
        }
    }

    //metodo para consultar los motivos de multa
    public List<MotivoMulta> consultarMotivos(MotivoMulta due) throws Exception {
        if (due.getDescripcion() == null
                || "".equals(due.getDescripcion().trim())) {
            throw new Exception("La lista esta vacia");

        } else {
            return this.due;
        }

    }

    //metodo para buscar un motivo de multa por su codigo
    public MotivoMulta codigoMotivo(short codigo) {
        for (int i = 0; i < due.size(); i++) {
            if (due.get(i).getCodigo() == codigo) {
                return due.get(i);
            }
        }
        return null;
    }

    //Metodo para modificar el valor de un motivo multa
    public void editMotivoMulta(int valor, short codigo) throws Exception {
        MotivoMulta found = codigoMotivo(codigo);
        if (found != null) {
            found.setValor(valor);
        } else {
            throw new Exception("El codigo del motivo de la multa no se encontro");
        }
    }

    public void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
