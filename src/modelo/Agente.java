package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author firofenix1986
 */
public class Agente extends Persona {

    private long placaAgente;

    public Agente(long placaAgente, long identificacion,
            String nombre, String apellido) throws Exception {
        super(identificacion, nombre, apellido);

//        if (nombre == null
//                || "".equals(nombre.trim())) {
//            throw new Exception(
//                    "El nombre del agente no puede estar vacio\n");
//        }
//        if (apellido == null
//                || "".equals(apellido.trim())) {
//            throw new Exception("El apellido del agente no puede estar vacio\n");
//        }
        setPlacaAgente(placaAgente);

    }

    public long getPlacaAgente() {
        return placaAgente;
    }

    public void setPlacaAgente(long placaAgente) throws Exception {
        if ((placaAgente + "").length() != 10) {

            throw new Exception("el numero de placa del agente debe contener \n"
                    + " " + "10 digitos\n");
        }
        this.placaAgente = placaAgente;
    }

    @Override
    public String toString() {
        return "Agente{" + "placaAgente=" + placaAgente + '}';
    }

}
